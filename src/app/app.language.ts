import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';


@Injectable()
export class LanguageService {

    constructor() { }

    //language support

    public engHeader =
        {
            selectLang:"Languages",logo: "assets/images/logo.svg", searchLine: "assets/images/searchLine.svg", whatLookingPlaceHolder: "What are you looking for...", logOrCreateAccount: "Log in or create your account",
            onTitle: "on Offeryep", aboutUs: "About Us", trustNSafe: "Trust & Safety", howWorks: "How It Works", needHelp: "Need Help?", logOut: "Logout",
            login: "Login", sellStuff: "Sell Your Stuff", myProfile: "My Profile", home: "Home", signUp: "Sign Up", backToTitle: "Back to Offeryep",
            logoPng: "assets/images/logo.svg", postIn30Secs: "Post your items in as little as 30 seconds", downloadTopRated: "Download our top-rated iOS or Android app to post your item",
            phoneNumber: "Phone Number", emailAddress: "Email Address", enterNumberCountryCode: "Please try entering your full number, including the country code.",
            send: "Send", continueWebsite: "Continue Browsing Website", thankYou: "Thank you", takeFewMinutes: "it may take a few minutes for you to receive it.",
            checkInternetConnection: "Please check your internet connection & try again.", ok: "Ok", sessionTimedOut: "Session Timed Out",
            timeToTitle: "Buy and sell quickly, safely and locally. It’s time to Offeryep!", titleTag: "Offeryep", discover:"Discover",
            languageOptions:[{lang:"Serbian",checked:false,value:1},
            {lang:"Montenegro",checked:false,value:2},
            {lang:"English",checked:false,value:3}],
            language:"Select Languages",Post_Product:"Post Product",addPhotos:"Add photos of your product",minimumPhotos:"At least one photo is mandatory", maximunPhotos:"Add upto more photo(optional)",maximumPhotoLength:"Product Image Maximum five...",what_selling:"What you are selling? ",Location:"Where is the product located?",price:"Price",
            Negotiable:"Negotiable",exchange:"Willing to exchange",not_found:"No post found",Category:"Category",
            selectCat:"Select Product Category",subCat:"Sub Category",selectSubCat:"Select Product Sub Category",
            select:"Select",post:"Post",Help:"Help",connectWith:"QUICKLY CONNECT WITH",continueFaceBook:"Continue with Facebook",
            continueGoogle:"Continue with Google",useMail:"OR USE YOUR EMAIL",Sign_Up:"Sign Up",Log_In:"Log In",
            byClicking:"By clicking on 'Sign up', you agree to the Offeryep",terms:"Terms & Conditions",
            policy:"Privacy Policy",and:"and",userName:"UserName",password:"PassWord",forgorPassword:"Forgot your password?",
            haveAccount:"Don't have an account?",email:"Email",Submit:"Submit",verifyCode:"Enter the verification code",
            enterOtp:"Enter the code that we sent to ",createAccount:"Create a new account",name:"Name",
            fullName:"fullname is missing",
            alreadyHaveAcc:"Already have an account?",productPrice:"Product Price",
            addTilte:"Add Title",
            description:"Description",whereSellItem:"Where you sell the item",IWant:"I Want..."
        }

    public engHome = {
        titleBuySellSafe: "Offeryep Post, Chat, Buy and Sell.", bringCommunities: "The simpler",to:'To',
        buySellFun: "way to buy and sell locally", cancel: "Cancel", reset: "Reset", location: "LOCATION",
        categories: "Categories", showMore: "Show More", showLess: "Show Less", distance: "DISTANCE", max: "Max", km: "Km",
        noCategory: "no category", sortBy: "SORT BY", newestFirst: "Newest First", closestFirst: "Closest First", lowToHighPrice: "Price: low to high",
        highToLowPrice: "Price: high to low", postedWithin: "POSTED WITHIN", allListings: "All listings", last24Hours: "The last 24 hours",
        last7Days: "The last 7 days", last30Days: "The last 30 days", filters: "Filters", price: "Price", currency: "Currency", from: "From",
        saveFilters: "Save Filters", mike: "assets/images/mike.svg", miles: "Km", noOffers: "No offers found", sorryNotFound: "Sorry, we couldn't find what you were looking for",
        downloadApp: "Download the app", startPostingOffers: "to start posting offers now", trustByMillions: "Trusted by Millions", blythe: "Blythe",
        robin: "Robin", chris: "Chris", greg: "Greg", heidi: "Heidi", krystal: "Krystal", readTitleStories: "Read Offeryep Stories", getApp: "Get the App!",
        joinMillions: "Join millions of loyal customers using the Offeryep mobile app, the simpler way to buy and sell locally!",
        googlePlayStore: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/google-play.svg",
        appStore: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/app-store.svg", loveJob: "Love your Job!",
        srAndroidEngineer: "Sr. Android Engineer", srIOSEngineer: "Sr. IOS Engineer", srBackendEngineer: "Sr. Backend Engineer",
        seePositions: "See All Positions!", sendAppLink: "Send app link", emailPlaceHolder: "your-email@domain.com",
        provideValidEmail: "Please provide a valid email address...", howPhone: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/howPhone.png",
        enterNumberCountryCode: "Please try entering your full number, including the country code.", recentRequestApp: "You have recently requested the app.",
        minsToReceive: "It may take a few minutes for you to receive it.", wait5Mins: "Please wait about 5 minutes", makeOtherRequest: "to make another request.",
        thankYou: "Thank you",changeLocation:"Change Location",subCat:"Sub Category",advanceFilter:"Advance Filter",setLocation:"Set Location",
        productCon:"Product Condition",selectProductCon:"Select Product Condition"
        ,other:"Other(see description)",forParts:"For Parts",used:"Used(normal wear)",
        openBox:"Open Box(never used)",reCon:"Reconditioned/Certified",new:"New(Never Used)",

    }

    public engAbout = {
        aboutUs: "About Us", howWorks: "How It Works", createCommunities: "We Bring People Together",
        comeTogetherSell: "to Discover Value", ourStory: "Our Story", ceo: "Nick Huzar, CEO", cto: "Arean van Veelen, CTO",
        betterBuySell: "A Better Way to Buy and Sell", largeMarket: "Offeryep is the largest mobile marketplace in the U.S.",
        topApp: "Top 3 App", shopCategory: "In Shopping Category", appYear: "App of Year", geekWire: "Geekwire", billion14: "$14 Billion",
        transactionYear: "In Transactions this Year", million23: "23+ Million", downloads: "Downloads", titleNews: "Offeryep In the News",
    }

    public engchangePassword = {
        changePassTitle: "Change Password - Offeryep", changePassword: "Change Password", currentPassword: "Current Password",
        newPasswordAgain: "New Password (again)", newPassword: "New Password", submit: "Submit",
    }

    public engDiscussions = {
        titleBuySellSimple: "Offeryep - Buy. Sell. Simple.", myOffers: "My Offers", keyBoard: "Key board", twoHundred: "200", $: "$",
        Jeffery: "Jeffery", ago: "ago", month: "month", one: "1", hey: "Hey", replyToss: "Reply to ss", send: "Send", report: "Report", sabari: "Sabari",
        whyreport: "Why do you want to report this item?", prohibited: "It's prohibited on Offeryep", offensive: "It's offensive to me",
        notReal: "It's not a real post", duplicate: "It's a duplicate post", wrongCategory: "It's in the wrong category", scam: "It may be a scam",
        stolen: "It may be stolen", other: "Other", additionalNote: "Additional note (optional)", writeNotehere: "Please write your note here...",
        done: "Done",

    }

    public engEmailVerify = {
        verifyEmailTitle: "Verify Email - Offeryep", emailVerify: "Email Verify",
        emailProvide: "Please provide a valid email address...",
        submit: "Submit",


    }

    public engFooter = {
        logoBottom: "assets/images/logo.svg", buySellSafe: "Post, Chat, Buy and Sell", downloadApp: "Download the app",
        footerFB: "assets/images/fb.svg", footerInsta: "assets/images/insta.svg", footerTwitter: "assets/images/twitter.svg", aboutUs: "About Us", howWorks: "How It Works", terms: "Terms",
        privacy: "Privacy", help: "Help", footerPlayStore: "assets/images/playstore.svg", footerAppStore: "assets/images/appstore.svg",

    }

    public engHowItWorks = {
        aboutUs: "About Us", howItWorks: "How It Works", mobileCommerce: "Buying and Selling Made Easy",
        startlessIn30Secs: "Get Started in Less Than 30 Seconds", discoverLocally: "Discover Locally",
        browseMillionListings: "Browse millions of listings to find amazing items nearby", chatInstantly: "Chat Instantly",
        messageUsersAnonymously: "Message users anonymously and securely through the Offeryep app. Our newsfeed allows you to connect with buyers, sellers and other people you know.",
        sellSimpleVideo: "Sell Quickly With Image", easilyPost: "Easily post your items for sale with just a simple snap chat from your smartphone.",
        buildTrustCommunity: "Building a Trusted Community", userProfiles: "User Profiles", knowMoreBuyers: "Know more about buyers and seller before you engage",
        userRatings: "User Ratings", checkBuyer: "Check buyer and seller ratings, and give ratings when you transact", realTimeAlerts: "Real-time Alerts",
        getNotified: "Get notified instantly on your phone when a buyer or seller contacts you",
        titleIs: "Offeryep is", toDownload: "to download", free: "free",
        downloadTopRated: "Download our top-rated iOS or Android app to get started today!", howItWorksPlayStore: "assets/images/playstore.svg",
        howItWorksAppstore: "assets/images/appstore.svg", comingSoon: "Coming Soon!!!", thankYou: "Thank you",
        fewMinsReceive: "it may take a few minutes for you to receive it.",

    }

    public engItem = {
        titleSimpleWayToBuy: "Offeryep is a simpler way to buy and sell locally. Get the free app.", itemAppStore: "assets/images/appstore.svg",
        itemPlayStore: "assets/images/playstore.svg", previous: "Previous", next: "Next", postRemoved: "post is removed", posted: "Posted",
        ago: "ago", report: "Report", description: "Description", comments: "Comments", condition: "Condition", viewAll: "view all",
        typeYourMessage: "Add Review...", youHave: "You have", one120: "120", characterLeft: "character left.",
        postComment: "Post Comment", myOtherOffers: "Related Post", price: "PRICE", $: "$", makeOffer: "Make Offer", follow: "Follow",
        following: "Following", unFollow: "Un-Follow", watch: "Favourite", watching: "Favourite", unWatch: "Un-Favourite", seller: "About the seller",
        approximationProtectPrivacy: "Approximation to protect seller's privacy", followers: "Followers", readyBuyHit: "Ready to buy? Hit the Buy button.",
        offer: "Offer", notSure: "Not sure?", ask: "Ask", forMoreInfo: "for more info", offerSent: "Offer Sent!", orBetterDownloadApp: "Or better yet download the app. It's faster!",
        getApp: "Get the app!",
        itemGooglePlay: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/google-play.svg",
        itemAppStore2: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/app-store.svg",  soldBy: "Sold by", whyReport: "Why do you want to report this item?",
        additionalNote: "Additional note (optional)", pleaseWriteNote: "Please write your note here...", done: "Done", reported: "Reported",
        thanksTakingTime: "Thank you for taking time to let us know.", send: "Send", nowFollowing: "You're now following", offers: "Offers",
        follower: "Follower", comingSoon: "Coming Soon!!!", thankYou: "Thank you", fewMinsReceive: "it may take a few minutes for you to receive it",
        enterQuestionPlaceHolder: "Enter your question here...", lengthTextarea120: "120 - lengthTextarea", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
        settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
        settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
        settingspayOff: "assets/images/paypal_off.svg",Sold:"Sold",follwo:"Followed by",Location:"Location",
        Negotiable:"Negotiable",notNegatiable:"Not Negotiable",edit:"Edit",delete:"Delete",
        Details:"Details",addReview:"Add Review",noOfferFound:"No Offer Found",
        delteListing:"Are you sure want to delete your listing?",Cancel:"Cancel",editProduct:"Edit Product Listing",
        addPhotos:"Add photos of your product",minimumPhotos:"At least one photo is mandatory", 
        maximunPhotos:"Add upto more photo(optional)",maximumPhotoLength:"Product Image Maximum five...",
        what_selling:"What you are selling? ",WhrerLocation:"Where is the product located?",negotiable:"Negotiable",exchange:"Willing to exchange",not_found:"No post found",Category:"Category",
        selectCat:"Select Product Category",subCat:"Sub Category", emailNotVerify:"Email Not Verified", googleCon:"Google+ Connected", googleDisCon:"Google+ Not Connected", 
        fbCon:"Facebook Connected", fbNotCon:"Facebook Not Connected",
        selectSubCat:"Select Product Sub Category",select:"Select",update:"Update", details:"Details",post:"Post"
    }

    public engLogin = {
        formBackground: "assets/images/formBackground.png", loginTo: "Login To", loginLogo: "assets/images/logo.svg", userName: "USER NAME",
        passWord: "PASSWORD", byClicking: "By Clicking on “Login” or ”Connect with Facebook” you shall agree to the Offeryep",
        termsService: "Terms of Service", privacyPolicy: "Privacy Policy", login: "Login", notRegistered: "Not registered?",
        signUp: "Sign up", forgotPassword: "Forgot password?", reset: "Reset", and: "and", registerErrMsg: "fdgfdgfdg",
        connectWith:"QUICKLY CONNECT WITH",continueFaceBook:"Continue with Facebook",continueGoogle:"Continue with Google",useMail:"OR USE YOUR EMAIL",
        alreadyHaveAcc:"Already have an account?",logFaceBook:"Login With Facebook?",faceBookLog:"Facebook Login",
    }

    public engLogout = {
    }

    public engMemberProfile = {
        profile: "Profile:", title: "Offeryep", offers: "Offers", $: "$", seller: "About the seller", followers: "Followers", follow: "Follow",
        following: "Following", unFollow: "Un-Follow", bangaluru: "Bangaluru", follower: "Follower",
        followBy:"Follwed By",sellerPolicy:"Approximation to protect seller's privacy"
    }

    public engneedHelp = {
        support: "Help - Offeryep",
    }

    public engPasswordReset = {
        forgotPasswordTitle: "Forgot Password - Offeryep", newPasswordEnter: "ENTER NEW PASSWORD", newPassword: "New Password",
        newPasswordAgain: "New Password (again)", success: "Success...", submit: "Submit",

    }

    public engPrivacy = {
        privacyPolicyTitle: "Privacy Policy - Offeryep",
    }

    public engRegister = {
        formBackground: "assets/images/formBackground.png", registerLogo: "assets/images/logo.svg", name: "NAME", fullNameMissing: "fullname is missing",
        userName: "USER NAME", email: "EMAIL", phoneNumber: "PHONE NUMBER", password: "PASSWORD",
        connectFBLogin: "By Clicking on “Login” or ”Connect with Facebook” you shall agree to the Offeryep", termsService: "Terms of Service",
        privacyPolicy: "Privacy Policy", and: "and", signUp: "SIGN UP", alreadyRegistered: "Already registered?", logIn: "Log In",
        signUpWith: "Sign Up with",connectWith:"QUICKLY CONNECT WITH",continueFaceBook:"Continue with Facebook",
        continueGoogle:"Continue with Google",useMail:"OR USE YOUR EMAIL",
        alreadyHaveAcc:"Already have an account?",logFaceBook:"Sign up With Facebook?",faceBookLog:"Facebook Sign up",
        Signup:"Sign up",createAccount:"Create a new account",
    }
    public engResetPassword = {
        passwordResetTitle: "Password reset - Offeryep", startBuyCrypto: "start buying and selling safe with crypto!",
        formBackground: "assets/images/formBackground.png", resetPasswordLogo: "assets/images/logo.svg", forgotPassword: "Forgot Password",
        recoverPassword: "Enter your email to recover your password", receiveLink: "You will receive a link to reset your password",
        email: "EMAIL", submit: "SUBMIT", resetPasswordlogoBottom: "assets/images/logo.svg",

    }

    public engSell = {
        sellTitle: "Offeryep - Buy. Sell. Simple.", browse: "Browse", noCategory: "no category", terms: "Terms", privacy: "Privacy",
        titleCopyRights: "© 2017 Offeryep, Inc.", sellStuff: "Sell your stuff",

    }
    public engSettings = {
        settingsTitle: "Account Settings - Offeryep", verifiedWith: "Verified With", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
        settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
        settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
        settingspayOff: "assets/images/paypal_off.svg", posts: "Posts", followers: "Followers", following: "Following", selling: "SELLING",
        sold: "SOLD", favourites: "FAVOURITES", noListYet: "NO LISTINGS (YET!)", $: "$",
        noAds:"No Ads Yet",snapSell:"Snap and post in 30 second to sell stuff!",
        noSold:"No Sold Yet",fountFav:"Found Your Favourite Yet",postAds:"Ads you love & make as 'favourite' will appear here",
        exchange:"you have exchanged your",for:"for",onSale:"on sale",follwing:"No Following Yet",
        internetErr:"Internet Error",internetSet:"Please check your internet settings", close:"Close",
        buildTrust:"Build Trust",cancel:"Cancel",payPal:"Connect Paypal",
        havePaypal:"If you already have a Paypal.me link add it here",Example:"Example",Save:"Save",
        editProfile:"Edit Profile",tapPhoto:"Tap the photo to change it",
        imageSize:"Images must be in PNG or JPG format and under 5mb",Name:"Name",userName:"User Name", 
        bio:"Bio",website:"Website",Location:"Location",privateInfo:"Private Information",emailAdd:"Email Address",
        phoneNumber:"Phone Number",
        Gender:"Gender", male:"Male", female:"Female",changePw:"Change Password", Cancel:"Cancel",
        edit:"edit", emailNotVerify:"Email Not Verified", googleCon:"Google+ Connected", 
        googleDisCon:"Google+ Not Connected", fbCon:"Facebook Connected", fbNotCon:"Facebook Not Connected"

    }

    public engTerms = {
    }

    public engTrust = {
        trustTitle: "Trust - Offeryep", trust: "Trust", trustworthiness: "The most valuable currency",
        communityWorld: "in our marketplace is trust", buildLocalMarket: "We’re building a local marketplace where the",
        wellBeing: "well-being of buyers and sellers comes first", obsessingText: "We want Offeryep to be a place where buying and selling can be more rewarding. We'll keep obsessing over every detailof the experience so our buyers and sellers can connect with more confidence. And we'll keep holding ourselvesand our community to high standards",
        trustNeighbours: "Earn trust from each other", careVigilant: "Get to know other users", userProfiles: "User Profiles",
        profileOpportunity: "Your profile is your opportunity to introduce yourself to other members of the community",
        verificationID: "ID Verification", secureIdentity: "We securely validate your identity using your state ID and Facebook profile",
        userRatings: "User Ratings", seeHowMany: "See how many completed transactions users have and check out their average rating",
        appMessaging: "In-App Messaging", securelyCommunicate: "Securely communicate with buyers and sellers without giving away personal information",
        winningGame: "Learn buying and selling best practices", buyingTips: "Buying Tips",
        coverBasics: "Cover the basics of how to successfully inspect and purchase items from sellers on Offeryep",
        sellingTips: "Selling Tips", overBasics: "over the basics of how to successfully engage with buyers on Offeryep",
        letUsKnow: "Let us know if we can help", customerExperts: "Customer Care Experts",
        hereToHelp: "We are here to help solve problems and investigate issues when they arise. Please email for assistance",
        workClosely: "We work closely with our Law Enforcement Partners to ensure incidents requiring further investigation are handledaccordingly",
        assistLaw: "To learn more about how we assist Law Enforcement Officers, please visit ourLaw Enforcement Resource Page",
        haveQuestions: "Still have questions?", visitOur: "Visit our", helpCenter: "Help Center", learnMore: "to learn more",


    }

    public engverifyEmail = {
        verifyEmailTitle: "Verify-email - Offeryep", congratulationsVerified: "Congratulations, your email Id has been verified",

    }
    


    //language support

    //Other Europe and Serbian Dinar Language

    public engHeader1 = {  languageOptions:  [{"lang":"Srpski","checked":false,"value":1},
    {"lang":"Crnogorski","checked":false,"value":2},
    {"lang":"Engleski","checked":false,"value":3}],language:"Jezik",discover:"Istražiti",
    Post_Product:"Objavite proizvod",addPhotos:"Dodajte slike proizvoda",
    minimumPhotos:"Jedna fotografija obavezna", maximunPhotos:"Dodajte još slika (nije obavezno)",maximumPhotoLength:"Moguće je dodati samo pet slika...",what_selling:"Šta prodajete?",Location:"Koja je lokacija proizvoda?",price:"Cijena",
    Negotiable:"Moguće pregovaranj",exchange:" ",not_found:"Objave nisu pronađene!",
    Category:"Kategorije",selectCat:"Molimo oberite kategoriju proizvoda",subCat:"Podkategorija",
    selectSubCat:"Dodajte podkategoriju proizvoda",select:"Odaberite",post:"Objavu",Help:"Pomoć",
    connectWith:"POVEŽITE SE BRZO SA",continueFaceBook:"Nastavite preko Facebook-a",
    continueGoogle:"Prijavite se preko Google-a",useMail:"ILI KORISTITE VAŠU E-MAIL ADRESU",
    Sign_Up:"Prijava",Log_In:"Prijava",byClicking:"Pritiskom na “Registrujte se” slažete se sa Offeryep",
    terms:"Pravila i uslovi",policy:"Politika privatnosti",and:"i",userName:"Korisničko ime",password:"Lozinka",
    forgorPassword:"Zaboravili ste lozinku?",haveAccount:"Nemate nalog?",email:"E-mail",Submit:"Potvrdite",
    verifyCode:"Unesite verifikacioni broj",enterOtp:"Unesite kod koji smo Vam poslali",
    createAccount:"Napravite novi nalog",name:"Ime",
    fullName:"Nedostaje puno ime",phoneNumber:"Broj telefona",productPrice:"Cena proizvoda",addTilte:"Dodati naslov",
    description:"Opis",whereSellItem:"Gde prodaješ stavku",
    alreadyHaveAcc:"Već imate nalog?",IWant:"Ja želim....",
    logo: "assets/images/logo.svg", searchLine: "assets/images/searchLine.svg", whatLookingPlaceHolder: "Pronađite Vaš proizvod", logOrCreateAccount: "Prijavite se ili kreirajte svoj nalog",
    onTitle: "na Offeryep", aboutUs: "O nama", trustNSafe: "Povjerenje i sigurnost", howWorks: "Način upotrebe", needHelp: "Da li Vam je potrebna pomoć?", logOut: "Odjavite se",
    login: "Registracija", sellStuff: "Prodajte Vaš proizvod", myProfile: "Vas profil", home: "Početna stranica", signUp: "Prijava", backToTitle: "Vratite se na Offeryep",
    logoPng: "assets/images/logo.svg", postIn30Secs: "Postavite Vaš proizvod za manje od 30 sekundi", downloadTopRated: "Preuzmite našu  iOS ili Android aplikaciju za postavljenje Vašeg proizvoda",
    emailAddress: "E-mail adresa", enterNumberCountryCode: "Molimo Vas, unesite Vaš broj telefona sa kodom države",
    send: "Poslati", continueWebsite: "Nastavite sa pretraživanjem Web stranice", thankYou: "Hvala", takeFewMinutes: "Za par minuta dobićete povratnu informaciju",
    checkInternetConnection: "Molimo Vas provjerite Vašu internet vezu & pokušajte ponovo", ok: "Ok", sessionTimedOut: "Vaša sesija je istekla",
    timeToTitle: "Kupujte i prodajte brzo, sigurno i lokalno. Vrijeme je za Offeryep !", titleTag: "Offeryep",
}
    // logo: "assets/images/logo.svg", searchLine: "assets/images/searchLine.svg", 
    // whatLookingPlaceHolder: "PronaÄ‘ite VaÅ¡ proizvod", 
    // logOrCreateAccount: "Prijavite se ili kreirajte svoj nalog", 
    // onTitle: "na Offeryep", aboutUs: "O nama", trustNSafe: "Povjerenje i sigurnost", 
    // howWorks: "NaÄin upotrebe", needHelp: "Da li Vam je potrebna pomoÄ‡?", logOut: "Odjavite se", 
    // login: "Registracija", sellStuff: "Prodajte VaÅ¡ proizvod", myProfile: "Vas profil", 
    // home: "PoÄetna stranica", signUp: "Prijava", backToTitle: "Vratite se na Offeryep", 
    // logoPng: "assets/images/logo.svg", postIn30Secs: "Postavite VaÅ¡ proizvod za manje od 30 sekundi", 
    // downloadTopRated: "Preuzmite naÅ¡u iOS ili Android aplikaciju za postavljenje VaÅ¡eg proizvoda",
    // emailAddress: "E-mail adresa", enterNumberCountryCode: "Molimo Vas, unesite VaÅ¡ broj telefona sa kodom drÅ¾ave", 
    // send: "Poslati", continueWebsite: "Nastavite sa pretraÅ¾ivanjem Web stranice", 
    // thankYou: "Hvala", takeFewMinutes: "Za par minuta dobiÄ‡ete povratnu informaciju", 
    // checkInternetConnection: "Molimo Vas provjerite VaÅ¡u internet vezu & pokuÅ¡ajte ponovo", ok: "Ok", 
    // sessionTimedOut: "VaÅ¡a sesija je istekla", timeToTitle: "Kupujte i prodajte brzo, sigurno i lokalno. Vrijeme je za Offeryep !", titleTag: "Offeryep", } 

public engHome1 = { changeLocation:"Promijeni lokaciju",subCat:"Podkategorija",advanceFilter:"Napredna pretraga",
setLocation:"Dodajte lokaciju",
titleBuySellSafe: "Offeryep postavi, ćaskaj, kupi i prodaj", bringCommunities: "Pojednostavi",
buySellFun: "Kako da kupite i prodate lokalno", cancel: "Otkažite", reset: "Počnite ponovo", location: "LOKACIJA",
categories: "Kategorije", showMore: "Prikaži više", showLess: "Prikaži manje", distance: "UDALJENOST", max: "Max", km: "Km",
noCategory: "Bez kategorije", sortBy: "SORTIRATI PO", newestFirst: "Najnovije prvo", closestFirst: "Najbliže prvo", lowToHighPrice: "Cijena: od najniže",
highToLowPrice: "Cijena: od najviše", postedWithin: "OBJAVLJENO SA", allListings: "Svi oglasi", last24Hours: "Posljednja 24 sata",
last7Days: "Posljednjih 7 dana", last30Days: "Posljednjih 30 dana", filters: "Filteri", price: "Cijena", currency: "Valuta", from: "Iz",
saveFilters: "Primijeni", mike: "assets/images/mike.svg", miles: "Km", noOffers: "Nema rezultata pretraživanja", sorryNotFound: "Nema rezultata Vaše pretrage",
downloadApp: "Preuzmite aplikaciju", startPostingOffers: "i postavite Vaše ponudu", trustByMillions: "Opravdano povjerenje više miliona korisnika", blythe: "Blythe",
robin: "Robin", chris: "Chris", greg: "Greg", heidi: "Heidi", krystal: "Krystal", readTitleStories: "Čitaj Offeryep Price", getApp: "Preuzmite aplikaciju",
joinMillions: "Pridružite se milionima lojalnih kupaca koristeći Offeryep mobilnu aplikaciju, najjednostavniji način da kupite i prodate lokalno!",
googlePlayStore: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/google-play.svg",
appStore: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/app-store.svg", loveJob: "Love your Job!",
srAndroidEngineer: "Sr. Android Engineer", srIOSEngineer: "Sr. IOS Engineer", srBackendEngineer: "Sr. Backend Engineer",
seePositions: "Pogledajte sve pozicije!", sendAppLink: "Posaljite link za aplikaciju", emailPlaceHolder: "vas-email@domain.com",
provideValidEmail: "Molimo Vas unesite važeću email adresu", howPhone: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/howPhone.png",
enterNumberCountryCode: "Molimo Vas unesite broj Vašeg telefona sa kodom države", recentRequestApp: "Vaš zahtjev za aplikaciju je već poslat",
minsToReceive: "Za par minuta dobićete povratnu informaciju", wait5Mins: "Molimo Vas sačekajte 5 minuta", makeOtherRequest: "da bi poslali drugi zahtjev",
thankYou: "Hvala Vam!",

// titleBuySellSafe: "Offeryep postavi, Ä‡askaj, kupi i prodaj", bringCommunities: "Pojednostavi", 
// buySellFun: "Kako da kupite i prodate lokalno", cancel: "OtkaÅ¾ite", reset: "PoÄnite ponovo", location: "LOKACIJA", 
// categories: "Kategorije", showMore: "PrikaÅ¾i viÅ¡e", showLess: "PrikaÅ¾i manje", distance: "UDALJENOST", 
// max: "Max", km: "Km", noCategory: "Bez kategorije", sortBy: "SORTIRATI PO", newestFirst: "Najnovije prvo", 
// closestFirst: "NajbliÅ¾e prvo", lowToHighPrice: "Cijena: od najniÅ¾e", highToLowPrice: "Cijena: od najviÅ¡e", 
// postedWithin: "OBJAVLJENO SA", allListings: "Svi oglasi", last24Hours: "Posljednja 24 sata", 
// last7Days: "Posljednjih 7 dana", last30Days: "Posljednjih 30 dana", filters: "Filteri", price: "Cijena", 
// currency: "Valuta", from: "Iz", saveFilters: "Primijeni", mike: "assets/images/mike.svg", miles: "Km", 
// noOffers: "Nema rezultata pretraÅ¾ivanja", sorryNotFound: "Nema rezultata VaÅ¡e pretrage", 
// downloadApp: "Preuzmite aplikaciju", startPostingOffers: "i postavite VaÅ¡e ponudu", 
// trustByMillions: "Opravdano povjerenje viÅ¡e miliona korisnika", blythe: "Blythe", robin: "Robin", chris: "Chris", 
// greg: "Greg", heidi: "Heidi", krystal: "Krystal", readTitleStories: "ÄŒitaj Offeryep Price", 
// getApp: "Preuzmite aplikaciju", joinMillions: "PridruÅ¾ite se milionima lojalnih kupaca koristeÄ‡i Offeryep mobilnu aplikaciju, najjednostavniji naÄin da kupite i prodate lokalno!", googlePlayStore: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/google-play.svg", appStore: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/app-store.svg", 
// loveJob: "Love your Job!", srAndroidEngineer: "Sr. Android Engineer", srIOSEngineer: "Sr. IOS Engineer", 
// srBackendEngineer: "Sr. Backend Engineer", seePositions: "Pogledajte sve pozicije!", 
// sendAppLink: "Posaljite link za aplikaciju", emailPlaceHolder: "vas-email@domain.com", 
// provideValidEmail: "Molimo Vas unesite vaÅ¾eÄ‡u email adresu", howPhone: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/howPhone.png", enterNumberCountryCode: "Molimo Vas unesite broj VaÅ¡eg telefona sa kodom drÅ¾ave", recentRequestApp: "VaÅ¡ zahtjev za aplikaciju je veÄ‡ poslat", minsToReceive: "Za par minuta dobiÄ‡ete povratnu informaciju", 
// wait5Mins: "Molimo Vas saÄekajte 5 minuta", makeOtherRequest: "da bi poslali drugi zahtjev", thankYou: "Hvala Vam!", 
} 

public engAbout1 = { 
//     aboutUs: "O nama", howWorks: "NaÄin upotrebe", createCommunities: "Mi spajamo ljude", 
// comeTogetherSell: "otkrijte valutu", ourStory: "NaÅ¡a priÄa", ceo: "Nick Huzar, CEO", cto: "Arean van Veelen, CTO", 
// betterBuySell: "Bolji naÄin za kupovinu i prodaju", 
// largeMarket: "Offeryep je najveÄ‡e mobilno trÅ¾iste u Crne Gore i Serbije", 
// topApp: "Tri najbolje aplikacije", shopCategory: "U kategoriji kupovine", 
// appYear: "Aplikacija godine", geekWire: "Geekwire", billion14: "$14 Biliona", 
// transactionYear: "transakcija ove godine", million23: "23+ Miliona", downloads: "Preuzimanja", 
// titleNews: "Offeryep u Novostima", 
    aboutUs: "O nama", howWorks: "Način upotrebe", createCommunities: "Mi spajamo ljude",
    comeTogetherSell: "otkrijte valutu", ourStory: "Naša priča", ceo: "Nick Huzar, CEO", cto: "Arean van Veelen, CTO",
    betterBuySell: "Bolji način za kupovinu i prodaju", largeMarket: "Offeryep je najveće mobilno tržiste u Crne Gore i Serbije",
    topApp: "Tri najbolje aplikacije", shopCategory: "U kategoriji kupovine", appYear: "Aplikacija godine", geekWire: "Geekwire", billion14: "$14 Biliona",
    transactionYear: "transakcija ove godine", million23: "23+ Miliona", downloads: "Preuzimanja", titleNews: "Offeryep u Novostima",
} 

public engchangePassword1 = { 
    // changePassTitle: "Promijenite lozinku - Offeryep", changePassword: "Promijenite lozinku", 
    // currentPassword: "Trenutna lozinka", newPasswordAgain: "Nova lozinka (ponovite)", 
    // newPassword: "Nova lozinka", submit: "Potvrdi", 
    changePassTitle: "Promijenite lozinku - Offeryep", changePassword: "Promijenite lozinku", currentPassword: "Trenutna lozinka",
    newPasswordAgain: "Nova lozinka (ponovite)", newPassword: "Nova lozinka", submit: "Potvrdi",
} 

public engDiscussions1 = { 
    // titleBuySellSimple: "Offeryep - Kupi. Prodaj. Jednostavno.", myOffers: "Moje Ponude", 
    // keyBoard: "Tastatura", twoHundred: "200", $: "$", Jeffery: "Jeffery", ago: "Prije", 
    // month: "Mjesec", one: "1", hey: "Pozdrav!", replyToss: "Odgovori ss", send: "Poslati", report: "Prijavite", 
    // sabari: "Sabari", whyreport: "ZaÅ¡to Å¾elite da prijavite ovaj proizvod?", 
    // prohibited: "Nije dozvoljeno na Offeryep", offensive: "Uvredljivo je za mene", notReal: "LaÅ¾an proizvod", 
    // duplicate: "Proizvod veÄ‡ postoji", wrongCategory: "Nalazi se u pogreÅ¡noj kategoriji", scam: "MoÅ¾e biti prevara", 
    // stolen: "MoÅ¾e biti ukraden", other: "Ostalo", additionalNote: "Napomena (nije obavezno)",
    // writeNotehere: "Molimo Vas ostavite VaÅ¡u poruku ovdje...", done: "ZavrÅ¡eno", 
    titleBuySellSimple: "Offeryep - Kupi. Prodaj. Jednostavno.", myOffers: "Moje Ponude", keyBoard: "Tastatura", twoHundred: "200", $: "$",
    Jeffery: "Jeffery", ago: "Prije", month: "Mjesec", one: "1", hey: "Pozdrav!", replyToss: "Odgovori ss", send: "Poslati", report: "Prijavite", sabari: "Sabari",
    whyreport: "Zašto želite da prijavite ovaj proizvod?", prohibited: "Nije dozvoljeno na Offeryep", offensive: "Uvredljivo je za mene",
    notReal: "Lažan proizvod", duplicate: "Proizvod već postoji", wrongCategory: "Nalazi se u pogrešnoj kategoriji", scam: "Može biti prevara",
    stolen: "Može biti ukraden", other: "Ostalo", additionalNote: "Napomena (nije obavezno)", writeNotehere: "Molimo Vas ostavite Vašu poruku ovdje...",
    done: "Završeno",

}

public engEmailVerify1 = { 
    // verifyEmailTitle: "Potvrdite E-mail â€“ Offeryep", 
    // emailVerify: "Potvrda E-maila", emailProvide: "Molimo Vas unesite vaÅ¾eÄ‡u email adresu", 
    // submit: "Potvrdi", 
    verifyEmailTitle: "Potvrdite E-mail – Offeryep", emailVerify: "Potvrda E-maila",
    emailProvide: "Molimo Vas unesite važeću email adresu",
    submit: "Potvrdi",
} 

public engFooter1 = { 
    // logoBottom: "assets/images/logo.svg", buySellSafe: "Postavite, Ä†askajte, Kupite i Prodajte",
    // downloadApp: "Preuzmite aplikaciju", footerFB: "assets/images/fb.svg", 
    // footerInsta: "assets/images/insta.svg", footerTwitter: "assets/images/twitter.svg", 
    // aboutUs: "O nama", howWorks: "NaÄin upotrebe", terms: "Uslovi", privacy: "Privatnost", 
    // help: "PomoÄ‡", footerPlayStore: "assets/images/playstore.svg", footerAppStore: "assets/images/appstore.svg", 
    logoBottom: "assets/images/logo.svg", buySellSafe: "Postavite, Ćaskajte, Kupite i Prodajte", downloadApp: "Preuzmite aplikaciju",
    footerFB: "assets/images/fb.svg", footerInsta: "assets/images/insta.svg", footerTwitter: "assets/images/twitter.svg", aboutUs: "O nama", howWorks: "Način upotrebe", terms: "Uslovi",
    privacy: "Privatnost", help: "Pomoć", footerPlayStore: "assets/images/playstore.svg", footerAppStore: "assets/images/appstore.svg",

} 

public engHowItWorks1 = { 
    // aboutUs: "O nama", howItWorks: "NaÄin upotrebe", mobileCommerce: "Kupujte i Prodajte lako", 
    // startlessIn30Secs: "PoÄinjete za manje od 30 sekundi", discoverLocally: "PretraÅ¾ite u VaÅ¡oj blizini", 
    // browseMillionListings: "PretraÅ¾ite milione oglasa da naÄ‘ete odgovarajuÄ‡e stvari u blizini", 
    // chatInstantly: "Ä†askajte odmah", 
    // messageUsersAnonymously: "PoÅ¡aljite poruku Älanovima anonimno i sigurno preko Offeryep aplikacije NaÅ¡e Novosti omoguÄ‡avaju Vam da se poveÅ¾ete sa kupcima, prodavcima i ostalim ljudima koje znate.", 
    // sellSimpleVideo: "Prodajte brzo postavljanjem slika", 
    // easilyPost: "Postavite VaÅ¡ proizvod lako pritiskom na dugme snap chata na VaÅ¡em telefonu.", 
    // buildTrustCommunity: "Gradimo pouzdano druÅ¡tvo", userProfiles: "KorisniÄki profil", 
    // knowMoreBuyers: "Saznajte viÅ¡e o kupcima i prodavcima prije nego se poveÅ¾ete", 
    // userRatings: "Ocjene korisnika", 
    // checkBuyer: "Provjerite ocjene kupca i prodavca i sami ocijenite nakon transakcije", 
    // realTimeAlerts: "Upozorenja", getNotified: "Dobijte obavjeÅ¡tenje odmah kada VaÅ¡ kontaktira kupac ili prodavac", 
    // titleIs: "Offeryep", toDownload: "moÅ¾ete preuzeti", free: "besplatno", 
    // downloadTopRated: "Preuzmite naÅ¡u iOS ili Android aplikaciju za postavljenje VaÅ¡eg proizvoda",
    // howItWorksPlayStore: "assets/images/playstore.svg", howItWorksAppstore: "assets/images/appstore.svg", 
    // comingSoon: "Uskoro !", thankYou: "Hvala", fewMinsReceive: "Za par minuta dobiÄ‡ete povratnu informaciju",
    aboutUs: "O nama", howItWorks: "Način upotrebe", mobileCommerce: "Kupujte i Prodajte lako",
    startlessIn30Secs: "Počinjete za manje od 30 sekundi", discoverLocally: "Pretražite u Vašoj blizini",
    browseMillionListings: "Pretražite milione oglasa da nađete odgovarajuće stvari u blizini", chatInstantly: "Ćaskajte odmah",
    messageUsersAnonymously: "Pošaljite poruku članovima anonimno i sigurno preko Offeryep aplikacije Naše Novosti omogućavaju Vam da se povežete sa kupcima, prodavcima i ostalim ljudima koje znate.",
    sellSimpleVideo: "Prodajte brzo postavljanjem slika", easilyPost: "Postavite Vaš proizvod lako pritiskom na dugme snap chata na Vašem telefonu.",
    buildTrustCommunity: "Gradimo pouzdano društvo", userProfiles: "Korisnički profil", knowMoreBuyers: "Saznajte više o kupcima i prodavcima prije nego se povežete",
    userRatings: "Ocjene korisnika", checkBuyer: "Provjerite ocjene kupca i prodavca i sami ocijenite nakon transakcije", realTimeAlerts: "Upozorenja",
    getNotified: "Dobijte obavještenje odmah kada Vaš kontaktira kupac ili prodavac",
    titleIs: "Offeryep", toDownload: "možete preuzeti", free: "besplatno",
    downloadTopRated: "Preuzmite našu  iOS ili Android aplikaciju za postavljenje Vašeg proizvoda", howItWorksPlayStore: "assets/images/playstore.svg",
    howItWorksAppstore: "assets/images/appstore.svg", comingSoon: "Uskoro !", thankYou: "Hvala",
    fewMinsReceive: "Za par minuta dobićete povratnu informaciju",
 } 

public engItem1 = { 
    productCon:"Stanje proizvoda",selectProductCon:"Izaberite Proizvodni uslov"
    ,other:"Ostalo (vidi opis)",forParts:"Za delove",used:"Koristi se (normalno habanje)",
    openBox:"Otvorena kutija (nikada se ne koristi)",reCon:"Obnovi / sertifikovani",new:"Novo (nikada korišćeno)",
    old:"Prodato",follwo:"Zapraćeni od",Location:"Lokacija",ago:"prije",
    Negotiable:"Moguće pregovaranje",notNegatiable:"Nije moguće pregovaranje",edit:"Uredite",
    delete:"Izbrišite",makeOffer:"Napravite ponudu",Details:"Detalji",addReview:"Dodajte komentar",noOfferFound:"Nema ponuda",
    delteListing:"Da li ste sigurno da želite da izbrišete Vaše oglase?",Cancel:"Otkažite",
    editProduct:"Uredite listu proizvoda",addPhotos:"Dodajte slike proizvoda",minimumPhotos:"Jedna fotografija obavezna", 
    maximunPhotos:"Dodajte još slika (nije obavezno)",maximumPhotoLength:"Moguće je dodati samo pet slika...",
    what_selling:"Šta prodajete? ",whereLocation:"Koja je lokacija proizvoda?",
    price:"Cijena",negotiable:"Moguće pregovaranje",
    exchange:"Moguća zamjena",not_found:"Objave nisu pronađene!",
    Category:"Kategorije",selectCat:"Molimo oberite kategoriju proizvoda",subCat:"Podkategorija",
    selectSubCat:"Dodajte podkategoriju proizvoda",select:"Odaberite",update:"Ažuriranje",

    titleSimpleWayToBuy: "Offeryep lakši način da kupujete i prodajete. Preuzmite aplikaciju.", itemAppStore: "assets/images/appstore.svg",
    itemPlayStore: "assets/images/playstore.svg", previous: "Prethodna", next: "Sljedeće", postRemoved: "oglas je izbrisan", posted: "Objavljeno",
    report: "Prijavite", description: "Opis", comments: "Komentar", condition: "Uslov", viewAll: "Pogledajte sve",
    typeYourMessage: "Ocijenite", youHave: "Preostalo Vam je još", one120: "120", characterLeft: "karaktera",
    postComment: "Postavite komentar", myOtherOffers: "Slični oglasi", follow: "Zapratiti",
    following: "Zapraćeni", unFollow: "Otpratiti", watch: "Omiljeno", watching: "Omiljeno", unWatch: "Uklonite iz Omiljeno", seller: "Podaci o prodavcu",
    approximationProtectPrivacy: "Zaštita privatnosti prodavca", followers: "Pratioci", readyBuyHit: "Spremni za kupovinu? Pritisnite dugme Kupi",
    offer: "Ponuda", notSure: "Niste sigurni?", ask: "Pitajte", forMoreInfo: "Više informacija", offerSent: "Ponuda poslata", orBetterDownloadApp: "Preuzmite aplikaciju ! Tražite brže!",
    getApp: "Preuzmite aplikaciju",
    itemGooglePlay: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/google-play.svg",
    itemAppStore2: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/app-store.svg",  soldBy: "Prodato od", whyReport: "Zašto želite da prijavite ovaj proizvod?",
    additionalNote: "Napomena (nije obavezno)", pleaseWriteNote: "Molimo Vas ostavite Vašu poruku ovdje...", done: "Završeno", reported: "Prijavljeno",
    thanksTakingTime: "Hvala Vam sto se izdvojili vrijeme i obavijestili nas!", send: "Poslato", nowFollowing: "Zapratili ste", offers: "Ponude",
    follower: "Pratilac", comingSoon: "Uskoro!!!", thankYou: "Hvala Vam", fewMinsReceive: "Za par minuta dobićete povratnu informaciju",
    enterQuestionPlaceHolder: "Unesite Vaše pitanje ovdje...", lengthTextarea120: "120 - lengthTextarea", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
    settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
    settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
    settingspayOff: "assets/images/paypal_off.svg",
    
    // titleSimpleWayToBuy: "Offeryep lakÅ¡i naÄin da kupujete i prodajete. Preuzmite aplikaciju.", 
    // itemAppStore: "assets/images/appstore.svg", itemPlayStore: "assets/images/playstore.svg", previous: "Prethodna", 
    // next: "SljedeÄ‡e", postRemoved: "oglas je izbrisan", posted: "Objavljeno", report: "Prijavite", description: "Opis", 
    // comments: "Komentar", condition: "Uslov", viewAll: "Pogledajte sve", typeYourMessage: "Ocijenite", 
    // youHave: "Preostalo Vam je joÅ¡", one120: "120", characterLeft: "karaktera", 
    // postComment: "Postavite komentar", myOtherOffers: "SliÄni oglasi", $: "$",  follow: "Zapratiti", 
    // following: "ZapraÄ‡eni", unFollow: "Otpratiti", watch: "Omiljeno", 
    // watching: "Omiljeno", unWatch: "Uklonite iz Omiljeno", seller: "Podaci o prodavcu", 
    // approximationProtectPrivacy: "ZaÅ¡tita privatnosti prodavca", followers: "Pratioci", 
    // readyBuyHit: "Spremni za kupovinu? Pritisnite dugme Kupi", offer: "Ponuda", 
    // notSure: "Niste sigurni?", ask: "Pitajte", forMoreInfo: "ViÅ¡e informacija", 
    // offerSent: "Ponuda poslata", orBetterDownloadApp: "Preuzmite aplikaciju ! TraÅ¾ite brÅ¾e!", 
    // getApp: "Preuzmite aplikaciju", details:"Detalji",
    // itemGooglePlay: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/google-play.svg", 
    // itemAppStore2: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/app-store.svg", soldBy: "Prodato od", 
    // whyReport: "ZaÅ¡to Å¾elite da prijavite ovaj proizvod?", additionalNote: "Napomena (nije obavezno)", 
    // pleaseWriteNote: "Molimo Vas ostavite VaÅ¡u poruku ovdje...", done: "ZavrÅ¡eno", reported: "Prijavljeno", 
    // thanksTakingTime: "Hvala Vam sto se izdvojili vrijeme i obavijestili nas!", send: "Poslato", 
    // nowFollowing: "Zapratili ste", offers: "Ponude", follower: "Pratilac", comingSoon: "Uskoro!!!", 
    // thankYou: "Hvala Vam", fewMinsReceive: "Za par minuta dobiÄ‡ete povratnu informaciju", 
    // enterQuestionPlaceHolder: "Unesite VaÅ¡e pitanje ovdje...", lengthTextarea120: "120 - lengthTextarea", 
    // settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg", 
    // settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", post:"pošta",
    // settingsEmailOn: "assets/images/Email_on.svg", settingsEmailOff: "assets/images/Email_off.svg", 
    // settingspayOn: "assets/images/paypal_on.svg", settingspayOff: "assets/images/paypal_off.svg" ,
    emailNotVerify:"E-pošta nije potvrđena", googleCon:"Google+ povezan", 
    googleDisCon:"Google+ nije povezan", fbCon:"FaceBook povezan", fbNotCon:"FaceBook nije povezan"
} 

public engLogin1 = { 
    // formBackground: "assets/images/formBackground.png", loginTo: "Prijavite se na", 
    // loginLogo: "assets/images/logo.svg", userName: "KORISNIÄŒKO IME", passWord: "LOZINKA", 
    // byClicking: "Pritiskom na â€œRegistracijaâ€ ili â€PoveÅ¾i se preko Facebook-aâ€ slaÅ¾ete se sa Offeryep uslovima", 
    // termsService: "Uslovi koriÅ¡Ä‡enja", privacyPolicy: "Privacy Policy", login: "Registracija", 
    // notRegistered: "Niste registrovani?", signUp: "Prijava", forgotPassword: "Zaboravili ste lozinku?", 
    // reset: "PoÄnite ponovo", and: "i", 

    formBackground: "assets/images/formBackground.png", loginTo: "Prijavite se na", loginLogo: "assets/images/logo.svg", userName: "KORISNIČKO IME",
    passWord: "LOZINKA", byClicking: "Pritiskom na “Registracija” ili ”Poveži se preko Facebook-a” slažete se sa Offeryep uslovima",
    termsService: "Uslovi  korišćenja", privacyPolicy: "Privacy Policy", login: "Registracija", notRegistered: "Niste registrovani?",
    signUp: "Prijava", forgotPassword: "Zaboravili ste lozinku?", reset: "Počnite ponovo", and: "i", registerErrMsg: "Greška ",
    connectWith:"POVEŽITE SE BRZO SA",continueFaceBook:"Prijavite se preko Facebook-a",
    continueGoogle:"Prijavite se preko Google-a",useMail:"ILI KORISTITE VAŠU E-MAIL ADRESU",
    alreadyHaveAcc:"Već imate nalog?",logFaceBook:"Prijavite se preko Facebook-a?",faceBookLog:"Prijavite se na Facebook", 
} 

public engLogout1 = { } 

public engMemberProfile1 = { 
    followBy:"Zapraćeni od",sellerPolicy:"Zaštita privatnosti prodavca",
    // profile: "Profil:", 
    // title: "Offeryep", offers: "Ponude", $: "$", seller: "O prodavcu", followers: "Pratioci", follow: "Zaprati", 
    // following: "ZapraÄ‡eni", unFollow: "Otpratiti", bangaluru: "Bangaluru", follower: "Pratilac", 
    profile: "Profil:", title: "Offeryep", offers: "Ponude", $: "$", seller: "O prodavcu", followers: "Pratioci", follow: "Zaprati",
    following: "Zapraćeni", unFollow: "Otpratiti", bangaluru: "Bangaluru", follower: "Pratilac",
} 

public engneedHelp1 = { 
    // support: "PomoÄ‡ - Offeryep", 
    support: "Pomoć - Offeryep",
}

public engPasswordReset1 = { 
    // forgotPasswordTitle: "Zaboravili ste lozinku- Offeryep", 
    // newPasswordEnter: "UNESITE NOVU LOZINKU", newPassword: "Nova lozinka", newPasswordAgain: "Nova lozinka (ponovite)", 
    // success: "Prihvaceno", submit: "Potvrdi", 
    forgotPasswordTitle: "Zaboravili ste lozinku- Offeryep", newPasswordEnter: "UNESITE NOVU LOZINKU", newPassword: "Nova lozinka",
    newPasswordAgain: "Nova lozinka (ponovite)", success: "Prihvaceno", submit: "Potvrdi",
} 

public engPrivacy1 = { 
    // privacyPolicyTitle: "Politika privatnosti- Offeryep", 
    privacyPolicyTitle: "Politika privatnosti- Offeryep",
} 

public engRegister1 = { 
    connectWith:"POVEŽITE SE BRZO SA",continueFaceBook:"Prijavite se preko Facebook-a",
    continueGoogle:" Prijavite se preko Google-a",useMail:"ILI KORISTITE VAŠU E-MAIL ADRESU",
    alreadyHaveAcc:"Već imate nalog?",
    logFaceBook:"Registrujte se preko Facebook-a”,faceBookLog:”Facebook registracija”,Signup:”Registrujte se",
    createAccount:"Napravite novi nalog",
    formBackground: "assets/images/formBackground.png", registerLogo: "assets/images/logo.svg", name: "IME", fullNameMissing: "Nedostaje Vaše puno ime",
    userName: "KORISNIČKO IME", email: "E-MAIL", phoneNumber: "BROJ TELEFONA", password: "LOZINKA",
    connectFBLogin: "By Clicking on “Login” or ”Pritiskom na “Registracija” ili ”Poveži se preko Facebook-a” slažete se sa Offeryep uslovima", termsService: "Uslovi  korišćenja",
    privacyPolicy: "Politika privatnosti", and: "i", signUp: "PRIJAVA", alreadyRegistered: "Već ste registrovani?", logIn: "Registracija",
    signUpWith: "Registruj se sa",
    // formBackground: "assets/images/formBackground.png", 
    // registerLogo: "assets/images/logo.svg", name: "IME", fullNameMissing: "Nedostaje VaÅ¡e puno ime", 
    // userName: "KORISNIÄŒKO IME", email: "E-MAIL", phoneNumber: "BROJ TELEFONA", password: "LOZINKA", 
    // connectFBLogin: "By Clicking on â€œLoginâ€ or â€Pritiskom na â€œRegistracijaâ€ ili â€PoveÅ¾i se preko Facebook-aâ€ slaÅ¾ete se sa Offeryep uslovima", 
    // termsService: "Uslovi koriÅ¡Ä‡enja", privacyPolicy: "Politika privatnosti", and: "i", 
    // signUp: "PRIJAVA", alreadyRegistered: "VeÄ‡ ste registrovani?", logIn: "Registracija", signUpWith: "Registruj se sa", 
} 

public engResetPassword1= { 
    // passwordResetTitle: "Ponovi lozinku- Offeryep", startBuyCrypto: "PoÄni da kupujeÅ¡ i prodajeÅ¡ uz crypto!", 
    // formBackground: "assets/images/formBackground.png", resetPasswordLogo: "assets/images/logo.svg", 
    // forgotPassword: "Zaboravili ste lozinku?", recoverPassword: "Unesite VaÅ¡u e-mail adresu kako bi dobili lozinku", 
    // email: "E-MAIL", submit: "POTVRDI", resetPasswordlogoBottom: "assets/images/logo.svg", 
    passwordResetTitle: "Ponovi lozinku- Offeryep", startBuyCrypto: "Počni da kupuješ i prodaješ uz crypto!",
    formBackground: "assets/images/formBackground.png", resetPasswordLogo: "assets/images/logo.svg", forgotPassword: "Zaboravili ste lozinku?",
    recoverPassword: "Unesite Vašu e-mail adresu kako bi dobili lozinku",
    email: "E-MAIL", submit: "POTVRDI", resetPasswordlogoBottom: "assets/images/logo.svg",

} 


public engSell1 = { 
    // sellTitle: "Offeryep â€“ Kupi, Prodaj, Jednostavno", browse: "TraÅ¾i", 
    // noCategory: "Bez kategorije", terms: "Uslovi", privacy: "Privatnost", 
    // titleCopyRights: "Â© 2018 Offeryep, Breezy d.o.o.", sellStuff: "Prodajte svoj proizvod", 
    sellTitle: "Offeryep – Kupi, Prodaj, Jednostavno", browse: "Traži", noCategory: "Bez kategorije", 
    terms: "Uslovi", privacy: "Privatnost",
    titleCopyRights: "© 2018 Offeryep, Breezy d.o.o.", sellStuff: "Prodajte svoj proizvod",
} 

public engSettings1 = { 
    noAds:"Još uvijek nema oglasa",snapSell:"Prodajte proizvod za 30 sekundi",
    noSold:"Još uvijek nema oglasa",fountFav:"Još uvijek nemate ništa pod Omiljeno!",
    postAds:"Oglase koje ste dodali u ‘’omiljeno’’ pojaviće se ovdje",exchange:"razmijenili ste",for:"za",
    onSale:"na prodaji",follwing:"Još uvijek nema pratilaca!",internetErr:"Internet Greška",
    internetSet:"Molimo Vas provjerite internet podešavanja", close:"Zatvorite",buildTrust:"Izgradite Povjerenje",
    cancel:"Otkažite",payPal:"Povežite PayPal",
    havePaypal:"Ukoliko već imate PayPal.me link, dodajte ga ovdje.",Example:"Primjer",Save:"Sačuvati",
    editProfile:"Uredite profil",tapPhoto:"Kliknite na sliku da je zamijenite",
    imageSize:"Slike moraju biti u PGN ili JPG formatu i manje od 5mb",Name:"Ime",userName:"Korisničko ime", 
    bio:"Biografija",website:"Web sajt",Location:"Lokacija",privateInfo:"Lične informacije",emailAdd:"E-mail adresa",
    phoneNumber:"Broj telefona",Gender:"Pol", male:"Muško", female:"Žensko",changePw:"Promijenite lozinku", 
    Cancel:"Otkažite",
    // settingsTitle: "Opcije Naloga â€“ Offeryep", verifiedWith: "PotvrÄ‘eno sa", 
    // settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg", 
    // settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", 
    // settingsEmailOn: "assets/images/Email_on.svg", settingsEmailOff: "assets/images/Email_off.svg", 
    // settingspayOn: "assets/images/paypal_on.svg", settingspayOff: "assets/images/paypal_off.svg", posts: "Oglasi", 
    // followers: "Pratioci", following: "ZapraÄ‡eni", selling: "PRODAJA", sold: "PRODATO", favourites: "LAJKOVI", 
    // noListYet: "NEMA OGLASA (JOS UVIJEK!)", $: "$", 
    settingsTitle: "Opcije Naloga – Offeryep", verifiedWith: "Potvrđeno sa", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
    settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
    settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
    settingspayOff: "assets/images/paypal_off.svg", posts: "Oglasi", followers: "Pratioci", following: "Zapraćeni", selling: "PRODAJA",
    sold: "PRODATO", favourites: "LAJKOVI", noListYet: "NEMA OGLASA (JOS UVIJEK!)", $: "$",
    edit:"Urediti", emailNotVerify:"E-pošta nije potvrđena", googleCon:"Google+ povezan", 
    googleDisCon:"Google+ nije povezan", fbCon:"FaceBook povezan", fbNotCon:"FaceBook nije povezan"
} 

public engTerms1 = { } 

public engTrust1 = { 
    trustTitle: "Povjerenje- Offeryep", trust: "Povjerenje", trustworthiness: "Najvrednija valuta",
        communityWorld: "povjerenje na našem tržištu", buildLocalMarket: "Mi gradimo lokalo tržište gdje",
        wellBeing: "Je prioritet prosperitet kupaca i prodavaca", obsessingText: "Želimo da Offeryep bude mjesto gdje su kupovina i prodaja satisfakcija. Nastavićemo da koristimo svaki detalj našeg iskustva kako bi se nasi kupci i prodavci povezivali sa više povjerenja. Gradimo društvo u skladu sa visokim standardima.",
        trustNeighbours: "Naučimo da vjerujemo jedni drugima", careVigilant: "Upoznajte naše korisnike", userProfiles: "Profili Korisnika",
        profileOpportunity: "Vaš profil je Vaša šansa da predstavite sebe zajednici",
        verificationID: "ID Verifikacija", secureIdentity: "Ažurirali smo Vaše podatke koristeći Vaš ID i Facebook profil",
        userRatings: "Ocjene korisnika", seeHowMany: "Pogledajte koliko korisnik ima izvršenih transakcija i provjerite ocjene korisnika",
        appMessaging: "İn-App Ćaskanje", securelyCommunicate: "Komunicirajte sigurno sa kupcima i prodavcima bez otkrivanja Vaših ličnih podataka",
        winningGame: "Naučite kako da najbolje kupujete i prodajete", buyingTips: "Savjeti pri kupovini",
        coverBasics: "Najbolji način da pregledate i kupite proizvode na Offeryep",
        sellingTips: "Savjeti prilikom prodaje", overBasics: "Najbolji način da se uspješno povežete sa kupcima na Offeryep",
        letUsKnow: "Obratite nam se za pomoć", customerExperts: "Stručnjaci za zaštitu potrošača",
        hereToHelp: "Obratite nam se kako bi riješili vaše probleme i pitanja. Pošaljite nam e-mail za pomoć.",
        workClosely: "Sarađujemo sa Pravnom Službom i zahtijevamo dalje istraživanje problema",
        assistLaw: "Kako bi saznali više o našoj saradnji sa Pravnim službenicima, molimo Vas posjetite našu stranicu Pravna Služba",
        haveQuestions: "İmate li još pitanja?", visitOur: "Visit our Posjetite našu", helpCenter: "Pomoć", learnMore: " da saznate više",
    // trustTitle: "Povjerenje- Offeryep", trust: "Povjerenje", trustworthiness: "Najvrednija valuta", 
    // communityWorld: "povjerenje na naÅ¡em trÅ¾iÅ¡tu", buildLocalMarket: "Mi gradimo lokalo trÅ¾iÅ¡te gdje", 
    // wellBeing: "Je prioritet prosperitet kupaca i prodavaca", 
    // obsessingText: "Å½elimo da Offeryep bude mjesto gdje su kupovina i prodaja satisfakcija. NastaviÄ‡emo da koristimo svaki detalj naÅ¡eg iskustva kako bi se nasi kupci i prodavci povezivali sa viÅ¡e povjerenja. Gradimo druÅ¡tvo u skladu sa visokim standardima.", trustNeighbours: "NauÄimo da vjerujemo jedni drugima", 
    // careVigilant: "Upoznajte naÅ¡e korisnike", userProfiles: "Profili Korisnika", profileOpportunity: "VaÅ¡ profil je VaÅ¡a Å¡ansa da predstavite sebe zajednici", 
    // verificationID: "ID Verifikacija", secureIdentity: "AÅ¾urirali smo VaÅ¡e podatke koristeÄ‡i VaÅ¡ ID i Facebook profil", 
    // userRatings: "Ocjene korisnika", seeHowMany: "Pogledajte koliko korisnik ima izvrÅ¡enih transakcija i provjerite ocjene korisnika", appMessaging: "Ä°n-App Ä†askanje", securelyCommunicate: "Komunicirajte sigurno sa kupcima i prodavcima bez otkrivanja VaÅ¡ih liÄnih podataka", winningGame: "NauÄite kako da najbolje kupujete i prodajete", 
    // buyingTips: "Savjeti pri kupovini", coverBasics: "Najbolji naÄin da pregledate i kupite proizvode na Offeryep", sellingTips: "Savjeti prilikom prodaje", overBasics: "Najbolji naÄin da se uspjeÅ¡no poveÅ¾ete sa kupcima na Offeryep", letUsKnow: "Obratite nam se za pomoÄ‡", customerExperts: "StruÄnjaci za zaÅ¡titu potroÅ¡aÄa", 
    // hereToHelp: "Obratite nam se kako bi rijeÅ¡ili vaÅ¡e probleme i pitanja. PoÅ¡aljite nam e-mail za pomoÄ‡.", workClosely: "SaraÄ‘ujemo sa Pravnom SluÅ¾bom i zahtijevamo dalje istraÅ¾ivanje problema", 
    // assistLaw: "Kako bi saznali viÅ¡e o naÅ¡oj saradnji sa Pravnim sluÅ¾benicima, molimo Vas posjetite naÅ¡u stranicu Pravna SluÅ¾ba", haveQuestions: "Ä°mate li joÅ¡ pitanja?", visitOur: "Visit our Posjetite naÅ¡u", helpCenter: "PomoÄ‡", learnMore: " da saznate viÅ¡e", 
} 

public engverifyEmail1 = { 
    verifyEmailTitle: "potvrdi-email - Offeryep", 
    congratulationsVerified: "ÄŒestitamo, potvrdili ste VaÅ¡u email adresu", 
} 

//     public engHeader1 = {  language:[{lang:"Srpski",checked:false,value:1},
//                            {lang:"Crnogorski",checked:false,value:2},
//                            {lang:"Engleski",checked:false,value:3}],discover:"otkriti",selectLang:"Jezik",logo: "assets/images/logo.svg", searchLine: "assets/images/searchLine.svg", whatLookingPlaceHolder: "PretraÅzi", logOrCreateAccount: "Prijavite se ili kreirajte svoj nalog", onTitle: "na Offeryep", aboutUs: "O nama", trustNSafe: "Povjerenje i sigurnost", howWorks: "NaÄin upotrebe", needHelp: "Da li Vam je potrebna pomoÄ‡?", logOut: "Odjavite se", login: "Prijavite se", sellStuff: "Prodajte Vaš proizvod", myProfile: "Vas profil", home: "PoÄetna stranica", signUp: "Prijavite se", backToTitle: "Vratite se na Offeryep", logoPng: "assets/images/logo.svg", postIn30Secs: "Postavite VaÅ¡ proizvod za manje od 30 sekundi", downloadTopRated: "Preuzmite naÅ¡u iOS ili Android aplikaciju za postavljenje VaÅ¡eg proizvoda", phoneNumber: "Broj telefona", emailAddress: "E-mail adresa", enterNumberCountryCode: "Molimo Vas, unesite VaÅ¡ broj telefona sa kodom drÅzave", send: "Poslati", continueWebsite: "Nastavite sa pretraÅzivanjem Web stranice", thankYou: "Hvala", takeFewMinutes: "Za par minuta dobiÄ‡ete povratnu informaciju", checkInternetConnection: "Molimo Vas provjerite VaÅ¡u internet vezu & pokuÅ¡ajte ponovo", ok: "Ok", sessionTimedOut: "VaÅ¡a sesija je istekla", timeToTitle: "Kupujte i prodajte brzo, sigurno i lokalno. Vrijeme je za Offeryep !", titleTag: "Offeryep", } 

// public engHome1 = { to:'Do',titleBuySellSafe: "Offeryep postavi, Ä‡askaj, kupi i prodaj", bringCommunities: "Pojednostavi", buySellFun: "Kako da kupite i prodate lokalno", cancel: "OtkaÅzi", reset: "PoÄni ponovo", location: "LOKACIJA", categories: "Kategorije", showMore: "PrikaÅzi viÅ¡e", showLess: "PrikaÅzi manje", distance: "UDALJENOST", max: "Max", km: "Km", noCategory: "Bez kategorije", sortBy: "SORTIRATI PO", newestFirst: "Najnovije prvo", closestFirst: "NajbliÅze prvo", lowToHighPrice: "Cijena: od najniÅze", highToLowPrice: "Cijena: od najviÅ¡e", postedWithin: "OBJAVLJENO SA", allListings: "Svi oglasi", last24Hours: "Posljednja 24 sata", last7Days: "Posljednjih 7 dana", last30Days: "Posljednjih 30 dana", filters: "Filteri", price: "Cijena", currency: "Valuta", from: "Od", saveFilters: "Primijeni", mike: "assets/images/mike.svg", miles: "Km", noOffers: "Nema rezultata pretraÅzivanja", sorryNotFound: "Nema rezultata VaÅ¡e pretrage", downloadApp: "Preuzmite aplikaciju", startPostingOffers: "i postavite VaÅ¡e ponudu", trustByMillions: "Opravdano povjerenje viÅ¡e miliona korisnika", blythe: "Blythe", robin: "Robin", chris: "Chris", greg: "Greg", heidi: "Heidi", krystal: "Krystal", readTitleStories: "ÄŒitaj Offeryep Price", getApp: "Preuzmite aplikaciju", joinMillions: "PridruÅzite se milionima lojalnih kupaca koristeÄ‡i Offeryep mobilnu aplikaciju, najjednostavniji naÄin da kupite i prodate lokalno!", googlePlayStore: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/google-play.svg", appStore: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/app-store.svg", loveJob: "Love your Job!", srAndroidEngineer: "Sr. Android Engineer", srIOSEngineer: "Sr. IOS Engineer", srBackendEngineer: "Sr. Backend Engineer", seePositions: "Pogledajte sve pozicije!", sendAppLink: "Posaljite link za aplikaciju", emailPlaceHolder: "vas-email@domain.com", provideValidEmail: "Molimo Vas unesite vaÅzeÄ‡u email adresu", howPhone: "https://d2qb2fbt5wuzik.cloudfront.net/Offeryepwebsite/images/howPhone.png", enterNumberCountryCode: "Molimo Vas unesite broj VaÅ¡eg telefona sa kodom drÅzave", recentRequestApp: "VaÅ¡ zahtjev za aplikaciju je veÄ‡ poslat", minsToReceive: "Za par minuta dobiÄ‡ete povratnu informaciju", wait5Mins: "Molimo Vas saÄekajte 5 minuta", makeOtherRequest: "da bi poslali drugi zahtjev", thankYou: "Hvala Vam!", } 

// public engAbout1 = { aboutUs: "O nama", howWorks: "NaÄin upotrebe", createCommunities: "Mi spajamo ljude", comeTogetherSell: "otkrijte valutu", ourStory: "NaÅ¡a priÄa", ceo: "Nick Huzar, CEO", cto: "Arean van Veelen, CTO", betterBuySell: "Bolji naÄin za kupovinu i prodaju", largeMarket: "Offeryep je najveÄ‡e mobilno trÅziste u Crne Gore i Serbije", topApp: "Tri najbolje aplikacije", shopCategory: "U kategoriji kupovine", appYear: "Aplikacija godine", geekWire: "Geekwire", billion14: "$14 Biliona", transactionYear: "transakcija ove godine", million23: "23+ Miliona", downloads: "Preuzimanja", titleNews: "Offeryep u Novostima", } 

// public engchangePassword1 = { changePassTitle: "Promijenite lozinku - Offeryep", changePassword: "Promijenite lozinku", currentPassword: "Trenutna lozinka", newPasswordAgain: "Nova lozinka (ponovite)", newPassword: "Nova lozinka", submit: "Potvrdi", } 

// public engDiscussions1 = { titleBuySellSimple: "Offeryep - Kupi. Prodaj. Jednostavno.", myOffers: "Moje Ponude", keyBoard: "Tastatura", twoHundred: "200", $: "$", Jeffery: "Jeffery", ago: "Prije", month: "Mjesec", one: "1", hey: "Pozdrav!", replyToss:" Odgovori ss", send: "Poslati", report: "Prijavite", sabari: "Sabari", whyreport: "ZaÅ¡to Åzelite da prijavite ovaj proizvod?", prohibited: "Nije dozvoljeno na Offeryep", offensive: "Uvredljivo je za mene", notReal: "LaÅzan proizvod", duplicate: "Proizvod veÄ‡ postoji", wrongCategory: "Nalazi se u pogreÅ¡noj kategoriji", scam: "MoÅze biti prevara", stolen: "MoÅze biti ukraden", other: "Ostalo", additionalNote: "Napomena (nije obavezno)", writeNotehere: "Molimo Vas ostavite VaÅ¡u poruku ovdje...", done: "ZavrÅ¡eno", } 

// public engEmailVerify1 = { verifyEmailTitle: "Potvrdite E-mail â€“ Offeryep", emailVerify: "Potvrda E-maila", emailProvide: "Molimo Vas unesite vaÅzeÄ‡u email adresu", submit: "Potvrdi", }

// public engFooter1 = { logoBottom: "assets/images/logo.svg", buySellSafe: "Postavite, Ćaskajte, Kupite i Prodajte", downloadApp: "Preuzmite aplikaciju", footerFB: "assets/images/fb.svg", footerInsta: "assets/images/insta.svg", footerTwitter: "assets/images/twitter.svg", aboutUs: "O nama", howWorks: "NaÄin upotrebe", terms: "Uslovi", privacy: "Privatnost", help: "PomoÄ‡", footerPlayStore: "assets/images/playstore.svg", footerAppStore: "assets/images/appstore.svg", } 

// public engHowItWorks1 = { aboutUs: "O nama", howItWorks: "NaÄin upotrebe", mobileCommerce: "Kupujte i Prodajte lako", startlessIn30Secs: "PoÄinjete za manje od 30 sekundi", discoverLocally: "PretraÅzite u VaÅ¡oj blizini", browseMillionListings: "PretraÅzite milione oglasa da naÄ‘ete odgovarajuÄ‡e stvari u blizini", chatInstantly: "Ćaskajte odmah", messageUsersAnonymously: "PoÅ¡aljite poruku Älanovima anonimno i sigurno preko Offeryep aplikacije NaÅ¡e Novosti omoguÄ‡avaju Vam da se poveÅzete sa kupcima, prodavcima i ostalim ljudima koje znate.", sellSimpleVideo: "Prodajte brzo postavljanjem slike", easilyPost: "Postavite VaÅ¡ proizvod lako pritiskom na dugme snap chata na VaÅ¡em telefonu.", buildTrustCommunity: "Gradimo pouzdano druÅ¡tvo", userProfiles: "KorisniÄki profil", knowMoreBuyers: "Saznajte viÅ¡e o kupcima i prodavcima prije nego se poveÅzete", userRatings: "Ocjene korisnika", checkBuyer: "Provjerite ocjene kupca i prodavca i sami ocijenite nakon transakcije", realTimeAlerts: "Upozorenja", getNotified: "Dobijte obavjeÅ¡tenje odmah kada VaÅ¡ kontaktira kupac ili prodavac", titleIs: "Offeryep", toDownload: "moÅzete preuzeti", free: "besplatno", downloadTopRated: "Preuzmite naÅ¡u iOS ili Android aplikaciju za postavljenje VaÅ¡eg proizvoda", howItWorksPlayStore: "assets/images/playstore.svg", howItWorksAppstore: "assets/images/appstore.svg", comingSoon: "Uskoro !", thankYou: "Hvala", fewMinsReceive: "Za par minuta dobiÄ‡ete povratnu informaciju", } 

// public engItem1 = { settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg",
// settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg",
// settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg",
// settingspayOff: "assets/images/paypal_off.svg",formBackground: "assets/images/formBackground.png", loginTo: "Login To", loginLogo: "assets/images/logo.svg", userName: "KORISNIÄŒKO IME", passWord: "LOZINKA", byClicking: "Pritiskom na â€œPrijavite seâ€ ili â€PoveÅzi se preko Facebook-aâ€ slaÅzete se sa Offeryep uslovima", termsService: "Uslovi koriÅ¡Ä‡enja", privacyPolicy: "Privacy Policy", login: "Prijavi se", notRegistered: "Not registered?", signUp: "Registrujte se", forgotPassword: "Zaboravili ste Å¡ifru?", reset: "Resetovati", and: "i", registerErrMsg: "GreÅ¡ka " } 

// public engLogout1 = { }
// public engLogin1 = { };
 
// public engMemberProfile1 = { profile: "Profil:", title: "Offeryep", offers: "Ponude", $: "$", seller: "O prodavcu", followers: "Pratioci", follow: "Zaprati", following: "ZapraÄ‡eno", unFollow: "Otpratiti", bangaluru: "Bangaluru", follower: "Pratilac", } 
// public engneedHelp1 = { support: "PomoÄ‡ - Offeryep", } 

// public engPasswordReset1 = { forgotPasswordTitle: "Zaboravili ste lozinku- Offeryep", newPasswordEnter: "UNESITE NOVU LOZINKU", newPassword: "Nova lozinka", newPasswordAgain: "Nova lozinka (ponovite)", success: "Prihvaceno", submit: "Potvrdi", } 

// public engPrivacy1 = { privacyPolicyTitle: "Politika privatnosti- Offeryep", } 

// public engRegister1 = { formBackground: "assets/images/formBackground.png", registerLogo: "assets/images/logo.svg", name: "IME", fullNameMissing: "Nedostaje VaÅ¡e puno ime", userName: "KORISNIÄŒKO IME", email: "E-MAIL", phoneNumber: "BROJ TELEFONA", password: "LOZINKA", connectFBLogin: "By Clicking on â€œLoginâ€ or â€Pritiskom na â€œPrijavite seâ€ ili â€PoveÅzi se preko Facebook-aâ€ slaÅzete se sa Offeryep uslovima", termsService: "Uslovi koriÅ¡Ä‡enja", privacyPolicy: "Politika privatnosti", and: "i", signUp: "REGISTRUJTE SE", alreadyRegistered: "VeÄ‡ ste registrovani?", logIn: "Prijavi se", signUpWith: "Registruj se sa", } 

// public engResetPassword1 = { passwordResetTitle: "Ponovi lozinku- Offeryep", startBuyCrypto: "PoÄni da kupujeÅ¡ i prodajeÅ¡ uz crypto!", formBackground: "assets/images/formBackground.png", resetPasswordLogo: "assets/images/logo.svg", forgotPassword: "Zaboravili ste lozinku?", recoverPassword: "Unesite VaÅ¡u e-mail adresu kako bi dobili lozinku", email: "E-MAIL", submit: "POTVRDI", resetPasswordlogoBottom: "assets/images/logo.svg", } 

// public engSell1 = { sellTitle: "Offeryep â€“ Kupi, Prodaj, Jednostavno", browse: "TraÅzi", noCategory: "Bez kategorije", terms: "Uslovi", privacy: "Privatnost", titleCopyRights: "Â© 2018 Offeryep, Breezy d.o.o.", sellStuff: "Prodajte svoj proizvod", } 

// public engSettings1 = { settingsTitle: "Opcije Naloga â€“ Offeryep", verifiedWith: "PotvrÄ‘eno sa", settingsFBOn: "assets/images/FB_on.svg", settingsFBOff: "assets/images/FB_off.svg", settingsGOn: "assets/images/G+_on.svg", settingsGOff: "assets/images/G+_off.svg", settingsEmailOn: "assets/images/Email_on.svg", settingsEmailOff: "assets/images/Email_off.svg", settingspayOn: "assets/images/paypal_on.svg", settingspayOff: "assets/images/paypal_off.svg", posts: "Oglasi", followers: "Pratioci", following: "ZapraÄ‡eno", selling: "PRODAJA", sold: "PRODATO", favourites: "OMILJENO", noListYet: "NEMA OGLASA (JOS UVIJEK!)", $: "$", } 

// public engTerms1 = { } 

// public engTrust1 = { trustTitle: "Povjerenje- Offeryep", trust: "Povjerenje", trustworthiness: "Najvrednija valuta", communityWorld: "povjerenje na naÅ¡em trÅziÅ¡tu", buildLocalMarket: "Mi gradimo lokalo trÅziÅ¡te gdje", wellBeing: "Je prioritet prosperitet kupaca i prodavaca", obsessingText: "Å½elimo da Offeryep bude mjesto gdje su kupovina i prodaja satisfakcija. NastaviÄ‡emo da koristimo svaki detalj naÅ¡eg iskustva kako bi se nasi kupci i prodavci povezivali sa viÅ¡e povjerenja. Gradimo druÅ¡tvo u skladu sa visokim standardima.", trustNeighbours: "NauÄimo da vjerujemo jedni drugima", careVigilant: "Upoznajte naÅ¡e korisnike", userProfiles: "Profili Korisnika", profileOpportunity: "VaÅ¡ profil je VaÅ¡a Å¡ansa da predstavite sebe zajednici", verificationID: "ID Verification", secureIdentity: "We securely validate your identity using your state ID and Facebook profile", userRatings: "User Ratings", seeHowMany: "See how many completed transactions users have and check out their average rating", appMessaging: "In-App Messaging", securelyCommunicate: "Securely communicate with buyers and sellers without giving away personal information", winningGame: "Learn buying and selling best practices", buyingTips: "Buying Tips", coverBasics: "Cover the basics of how to successfully inspect and purchase items from sellers on Offeryep", sellingTips: "Selling Tips", overBasics: "over the basics of how to successfully engage with buyers on Offeryep", letUsKnow: "Let us know if we can help", customerExperts: "Customer Care Experts", hereToHelp: "We are here to help solve problems and investigate issues when they arise. Please email for assistance", workClosely: "We work closely with our Law Enforcement Partners to ensure incidents requiring further investigation are handledaccordingly", assistLaw: "To learn more about how we assist Law Enforcement Officers, please visit ourLaw Enforcement Resource Page", haveQuestions: "Still have questions?", visitOur: "Visit our", helpCenter: "Help Center", learnMore: "to learn more", } 

// public engverifyEmail1 = { verifyEmailTitle: "Verify-email - Offeryep", congratulationsVerified: "Congratulations, your email Id has been verified", } 



}